import java.nio.file.Paths

apply plugin: 'com.android.application'

android {
    compileSdkVersion 31
    buildToolsVersion "31.0.0"
    signingConfigs {
        def keystorePwd = null
        def alias = null
        def pwd = null
        if (project.rootProject.file('local.properties').exists()) {
            Properties properties = new Properties()
            properties.load(project.rootProject.file('local.properties').newDataInputStream())
            keystorePwd = properties.getProperty("RELEASE_STORE_PASSWORD")
            alias = properties.getProperty("RELEASE_KEY_ALIAS")
            pwd = properties.getProperty("RELEASE_KEY_PASSWORD")
        }
        release {
            storeFile file("hitokoto.jks")
            storePassword keystorePwd != null ? keystorePwd : System.getenv("KEYSTORE_PASS")
            keyAlias alias != null ? alias : System.getenv("ALIAS_NAME")
            keyPassword pwd != null ? pwd : System.getenv("ALIAS_PASS")
        }
    }
    defaultConfig {
        applicationId "io.github.nekoinverter.ehviewer"
        minSdkVersion 23
        targetSdkVersion 31
        versionCode 154
        versionName "1.7.24.2"
        resConfigs "zh", "zh-rCN", "zh-rHK", "zh-rTW",
                "es", "ja", "ko", "fr", "de", "th"
        testOptions.unitTests.includeAndroidResources = true
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_11
        targetCompatibility JavaVersion.VERSION_11
    }

    lintOptions {
        disable 'MissingTranslation'
        abortOnError true
        checkReleaseBuilds false
    }

    packagingOptions {
        exclude 'META-INF/**'
        exclude 'kotlin/**'
        exclude 'org/**'
        exclude '**.properties'
        exclude '**.bin'
    }

    dependenciesInfo.includeInApk false

    buildTypes {
        release {
            minifyEnabled true
            shrinkResources true
            proguardFiles 'proguard-rules.pro'
            signingConfig signingConfigs.release
        }
        debug {
            applicationIdSuffix ".debug"
            signingConfig signingConfigs.release
        }
    }

    sourceSets {
        main {
            java.srcDirs += 'src/main/java-gen'
        }
    }
}

tasks.withType(JavaCompile) {
    task -> task.dependsOn ":daogenerator:executeDaoGenerator"
}

def optimizeReleaseRes = task('optimizeReleaseRes').doLast {
    def aapt2 = Paths.get(project.android.sdkDirectory.path, 'build-tools', project.android.buildToolsVersion, 'aapt2')
    def zip = Paths.get(
            project.buildDir.path,
            "intermediates",
            "optimized_processed_res",
            "release",
            "resources-release-optimize.ap_"
    )
    def optimized = new File("${zip}.opt")
    def cmd = exec {
        commandLine aapt2, 'optimize', '--collapse-resource-names',
                '-o', optimized, zip
        ignoreExitValue false
    }
    if (cmd.exitValue == 0) {
        delete(zip)
        optimized.renameTo("$zip")
    }
}

tasks.whenTaskAdded { task ->
    if (task.name == 'optimizeReleaseResources') {
        task.finalizedBy optimizeReleaseRes
    }
}

dependencies {
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    implementation 'androidx.activity:activity:1.2.3'
    implementation 'androidx.biometric:biometric:1.1.0'
    implementation 'androidx.browser:browser:1.3.0'
    implementation 'androidx.cardview:cardview:1.0.0'
    implementation 'androidx.core:core:1.6.0'
    implementation 'androidx.dynamicanimation:dynamicanimation:1.1.0-alpha03'
    implementation 'androidx.fragment:fragment:1.3.5'
    implementation 'androidx.preference:preference:1.1.1'
    implementation 'androidx.recyclerview:recyclerview:1.2.1'
    implementation 'androidx.swiperefreshlayout:swiperefreshlayout:1.2.0-alpha01'
    implementation 'androidx.webkit:webkit:1.4.0'
    implementation 'com.drakeet.drawer:drawer:1.0.3'
    implementation 'com.google.android.material:material:1.4.0'
    implementation 'com.getkeepsafe.relinker:relinker:1.4.4'
    implementation 'com.getkeepsafe.taptargetview:taptargetview:1.13.3'
    implementation 'com.github.seven332.a7zip:extract-lite:cba0ce9586'
    implementation 'com.github.seven332:beerbelly:0.1.4'
    implementation 'com.github.seven332:glgallery:0.1.2'
    implementation 'com.github.seven332:glview:0.1.0'
    implementation 'com.github.seven332:glview-image:0.1.0'
    implementation 'com.github.seven332:streampipe:0.1.0'
    implementation 'com.github.seven332:tuxiang:0.1.6'
    implementation 'com.github.seven332:unifile:9ec57bcd8f'
    implementation 'com.github.seven332:yorozuya-thread:0.1.1'
    implementation 'com.github.seven332:yorozuya-collect:0.1.4'
    implementation 'com.h6ah4i.android.widget.advrecyclerview:advrecyclerview:1.0.0'
    implementation 'com.microsoft.appcenter:appcenter-analytics:4.2.0'
    implementation 'com.microsoft.appcenter:appcenter-crashes:4.2.0'
    implementation 'com.microsoft.appcenter:appcenter-distribute:4.2.0'
    implementation 'com.squareup.okhttp3:okhttp:4.9.1'
    implementation 'com.squareup.okhttp3:okhttp-dnsoverhttps:4.9.1'
    implementation 'com.takisoft.preferencex:preferencex:1.1.0'
    implementation 'com.github.NekoInverter:Image:8fd6c1e1df'
    implementation 'com.gitlab.NekoInverter:yorozuya:723dc867a9'
    implementation 'dev.rikka.rikkax.appcompat:appcompat:1.2.0-rc01'
    implementation 'dev.rikka.rikkax.core:core:1.3.2'
    implementation 'dev.rikka.rikkax.insets:insets:1.1.0'
    implementation 'dev.rikka.rikkax.material:material:1.6.5'
    implementation 'dev.rikka.rikkax.recyclerview:recyclerview-ktx:1.2.2'
    implementation 'dev.rikka.rikkax.widget:borderview:1.0.1'
    implementation 'dev.rikka.rikkax.preference:simplemenu-preference:1.0.3'
    implementation 'org.ccil.cowan.tagsoup:tagsoup:1.2.1'
    implementation 'org.greenrobot:greendao:3.3.0'
    implementation 'org.jsoup:jsoup:1.13.1'
    testImplementation 'junit:junit:4.12'
    testImplementation 'org.robolectric:robolectric:4.2.1'
    testImplementation 'org.jooq:joor:0.9.6'
}

configurations.all {
    resolutionStrategy {
        force 'com.github.seven332:glgallery:25893283ca'
        force 'com.github.seven332:glview:ba6aee61d7'
        force 'com.github.seven332:glview-image:68d94b0fc2'

        exclude group: 'com.github.seven332', module: 'okhttp'
        exclude group: 'com.github.seven332', module: 'image'
        exclude group: 'com.github.seven332', module: 'yorozuya'
        exclude group: 'androidx.appcompat', module: 'appcompat'
    }
}
